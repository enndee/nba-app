import React, { Component } from "react";
import { Route, Redirect, Switch, withRouter } from "react-router-dom";
import { getTeams } from "./services/teamService";
import NavBar from "./components/navBar";
import ConferenceSelect from "./components/conferenceSelect";
import TeamInfo from "./components/teamInfo";
import PlayerSearch from "./components/playerSearch";
import Loading from "./components/loading";
import ConnectionError from "./components/connectionError";
import NotFound from "./components/notFound";

class App extends Component {
  state = { teams: [], isLoaded: false, isError: false, errorMessage: null };

  async componentDidMount() {
    document.title = "NBA App";

    try {
      const { data: teams } = await getTeams();

      teams.map(team => {
        team.slug = this.getSlug(team.name);
        team.logo = `${process.env.PUBLIC_URL}/logos/${team.slug}-logo.svg`;
        return team;
      });

      this.setState({ teams, isLoaded: true });
    } catch (ex) {
      this.setState({
        isLoaded: true,
        isError: true,
        errorMessage: ex.message
      });
      console.log(ex);
    }
  }

  getSlug(teamName) {
    return teamName
      .split(" ")
      .join("-")
      .toLowerCase();
  }

  getCurrentPagePathname() {
    const slug = this.props.location.pathname.substring(
      this.props.location.pathname.lastIndexOf("/") + 1
    );
    return slug.length === 0 ? "home-page" : slug;
  }

  render() {
    const { isLoaded, teams, isError, errorMessage } = this.state;

    if (isLoaded && !isError) {
      return (
        <div className={this.getCurrentPagePathname()}>
          <NavBar />
          <main className="container">
            <Switch>
              <Route path="/teams/:slug" component={TeamInfo} />
              <Redirect from="/teams" to="/" />
              <Route path="/player-search" component={PlayerSearch} />
              <Route path="/not-found" component={NotFound} />
              <Route
                path="/"
                exact
                render={props => <ConferenceSelect {...props} teams={teams} />}
              />
              <Redirect to="/not-found" />
            </Switch>
          </main>
        </div>
      );
    } else if (isError) {
      return <ConnectionError errorMessage={errorMessage} />;
    } else {
      return <Loading />;
    }
  }
}

export default withRouter(App);
