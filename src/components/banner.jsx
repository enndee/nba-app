import React from "react";

const Banner = ({ logo }) => {
  return (
    <div className="team-logo">
      <img src={logo} alt="" />
    </div>
  );
};

export default Banner;
