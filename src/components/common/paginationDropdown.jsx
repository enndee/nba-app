import React from "react";
import _ from "lodash";
import PropTypes from "prop-types";

const PaginationDropdown = ({
  itemsCount,
  pageSize,
  currentPage,
  onPageChange
}) => {
  const pagesCount = Math.ceil(itemsCount / pageSize);
  if (pagesCount <= 1) return null;

  const pages = _.range(1, pagesCount + 1);
  return (
    <nav>
      <ul className="pagination">
        <li>
          <button
            disabled={currentPage === 1 ? true : null}
            className={currentPage === 1 ? "disabled" : null}
            onClick={() => onPageChange(currentPage - 1)}
          >
            Previous
          </button>
        </li>
        <li className="dropdown">
          <label htmlFor="page-dropdown">Page</label>
          <select
            id="page-dropdown"
            value={currentPage}
            onChange={event => onPageChange(Number(event.target.value))}
          >
            {pages.map(page => (
              <option key={page} value={page}>
                {page}
              </option>
            ))}
          </select>
          of {pagesCount}
        </li>
        <li>
          <button
            disabled={currentPage === pagesCount ? true : null}
            className={currentPage === pagesCount ? "disabled" : null}
            onClick={() => onPageChange(currentPage + 1)}
          >
            Next
          </button>
        </li>
      </ul>
    </nav>
  );
};

PaginationDropdown.propTypes = {
  itemsCount: PropTypes.number.isRequired,
  pageSize: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired
};

export default PaginationDropdown;
