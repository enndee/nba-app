import React, { Component } from "react";
import _ from "lodash";
import PropTypes from "prop-types";

class PaginationDropdown extends Component {
  state = { value: 1 };

  handleChange = event => {
    this.setState({ value: event.target.value });
    this.props.onPageChange(Number(event.target.value));
  };

  handleButtonClick = page => {
    this.setState({ value: page });
    this.props.onPageChange(page);
  };

  render() {
    const { itemsCount, pageSize, currentPage } = this.props;
    const { value } = this.state;

    const pagesCount = Math.ceil(itemsCount / pageSize);
    if (pagesCount <= 1) return null;

    const pages = _.range(1, pagesCount + 1);

    return (
      <nav>
        <ul className="pagination">
          <li>
            <button
              disabled={currentPage === 1 ? true : null}
              className={currentPage === 1 ? "disabled" : null}
              onClick={() => this.handleButtonClick(currentPage - 1)}
            >
              Previous
            </button>
          </li>
          <li className="dropdown">
            <label htmlFor="page-dropdown">Page</label>
            <select
              id="page-dropdown"
              value={value}
              onChange={this.handleChange}
            >
              {pages.map(page => (
                <option key={page} value={page}>
                  {page}
                </option>
              ))}
            </select>
            of {pagesCount}
          </li>
          <li>
            <button
              disabled={currentPage === pagesCount ? true : null}
              className={currentPage === pagesCount ? "disabled" : null}
              onClick={() => this.handleButtonClick(currentPage + 1)}
            >
              Next
            </button>
          </li>
        </ul>
      </nav>
    );
  }
}

PaginationDropdown.propTypes = {
  itemsCount: PropTypes.number.isRequired,
  pageSize: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired
};

export default PaginationDropdown;
