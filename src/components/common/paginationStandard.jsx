import React from "react";
import PropTypes from "prop-types";
import { paginationLinks } from "../../utils/paginationLinks";

// Standard pagination - 'First', 'Last', 'Previous', 'Next' and page number links.
const PaginationStandard = props => {
  const {
    itemsCount,
    pageSize,
    currentPage,
    paginationRange,
    onPageChange
  } = props;

  const pagesCount = Math.ceil(itemsCount / pageSize);
  if (pagesCount <= 1) return null;

  let pages = paginationLinks(currentPage, pagesCount, paginationRange);

  return (
    <nav>
      <ul className="pagination">
        <li>
          <button
            disabled={currentPage === 1 ? true : null}
            className={currentPage === 1 ? "disabled" : null}
            onClick={() => onPageChange(1)}
          >
            First
          </button>
        </li>
        <li>
          <button
            disabled={currentPage === 1 ? true : null}
            className={currentPage === 1 ? "disabled" : null}
            onClick={() => onPageChange(currentPage - 1)}
          >
            Previous
          </button>
        </li>
        {pages.map(page => (
          <li
            key={page}
            className={page === currentPage ? "page-item active" : "page-item"}
          >
            <button onClick={() => onPageChange(page)}>
              {page}
              <span className="screen-reader-text">
                {page === currentPage ? "current page" : null}
              </span>
            </button>
          </li>
        ))}
        <li>
          <button
            disabled={currentPage === pagesCount ? true : null}
            className={currentPage === pagesCount ? "disabled" : null}
            onClick={() => onPageChange(currentPage + 1)}
          >
            Next
          </button>
        </li>
        <li>
          <button
            disabled={currentPage === pagesCount ? true : null}
            className={currentPage === pagesCount ? "disabled" : null}
            onClick={() => onPageChange(pagesCount)}
          >
            Last
          </button>
        </li>
      </ul>
    </nav>
  );
};

PaginationStandard.propTypes = {
  itemsCount: PropTypes.number.isRequired,
  pageSize: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired
};

PaginationStandard.defaultProps = {
  paginationRange: 5
};

export default PaginationStandard;
