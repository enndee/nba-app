import React from "react";
import PropTypes from "prop-types";

const SearchBox = ({ id, label, placeholder, value, onChange, ...rest }) => {
  return (
    <React.Fragment>
      <label id={`${id}-label`} htmlFor={id}>
        {label}
      </label>
      <input
        id={id}
        className="input-field search-box"
        placeholder={placeholder}
        value={value}
        onChange={e => onChange(e.currentTarget.value)}
        {...rest}
      />
    </React.Fragment>
  );
};

SearchBox.propTypes = {
  id: PropTypes.string.isRequired
};

export default SearchBox;
