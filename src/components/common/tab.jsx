import React from "react";

const Tab = ({
  tab,
  selectedTab,
  onTabSelect,
  onKeyDown,
  tabRef,
  tabNumber
}) => {
  const selected = tab === selectedTab ? true : false;

  return (
    <button
      role="tab"
      aria-controls={`tabpanel-${tab.id.toLowerCase()}`}
      id={`tab-${tab.id.toLowerCase()}`}
      className={`tab ${selected ? "active" : "inactive"}`}
      aria-selected={selected ? "true" : "false"}
      tabIndex={selected ? null : "-1"}
      onClick={() => onTabSelect(tab)}
      onKeyDown={e => onKeyDown(e, tabNumber)} // Pass the event (so we can determine which button was pressed) and the number of the tab
      ref={tabRef}
    >
      {tab.tabText}
    </button>
  );
};

export default Tab;
