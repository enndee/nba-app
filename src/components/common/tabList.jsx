import React, { Component } from "react";
import Tab from "./tab";

class TabList extends Component {
  state = { tabRefs: [] };

  componentDidMount() {
    const tabRefs = this.createReferences();
    this.setState({ tabRefs });
  }

  createReferences = () => {
    let tabRefs = [];
    for (let i = 0; i < this.props.tabs.length; i++) {
      tabRefs[i] = React.createRef();
    }

    return tabRefs;
  };

  handleKeyDown = (e, tabNumber) => {
    const { tabRefs } = this.state;
    const { keyCode: key } = e;

    // Key codes for the back and forward arrow keys as well as 'home' and 'end'
    const back = 37;
    const forward = 39;
    const home = 36;
    const end = 35;

    if (key === back && tabNumber > 0) tabRefs[tabNumber - 1].current.focus();
    else if (key === forward && tabNumber < tabRefs.length - 1)
      tabRefs[tabNumber + 1].current.focus();
    else if (key === home) tabRefs[0].current.focus();
    else if (key === end) tabRefs[tabRefs.length - 1].current.focus();
  };

  render() {
    const { tabs, selectedTab, onTabSelect, ariaLabel } = this.props;
    const { tabRefs } = this.state;

    return (
      <div role="tablist" aria-label={ariaLabel} className="tablist">
        {tabs.map((tab, index) => (
          <Tab
            tab={tab}
            key={tab.id}
            selectedTab={selectedTab}
            onTabSelect={onTabSelect}
            onKeyDown={this.handleKeyDown}
            tabRef={tabRefs[index]}
            tabNumber={index}
          />
        ))}
      </div>
    );
  }
}

export default TabList;
