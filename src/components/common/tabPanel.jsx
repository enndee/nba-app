import React from "react";

const TabPanel = ({ tab, selectedTab }) => {
  return (
    <div
      tabIndex="0"
      role="tabpanel"
      id={`tabpanel-${tab.id.toLowerCase()}`}
      aria-labelledby={`tab-${tab.id.toLowerCase()}`}
      className={`tabpanel ${tab.id === selectedTab.id ? "show" : "hide"}`}
    >
      {tab.content}
    </div>
  );
};

export default TabPanel;
