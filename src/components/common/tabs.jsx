import React, { Component } from "react";
import Tablist from "./tabList";
import TabPanel from "./tabPanel";
import PropTypes from "prop-types";

class Tabs extends Component {
  state = { selectedTab: this.props.tabs[0] };

  handleTabSelect = selectedTab => {
    this.setState({ selectedTab });
  };

  render() {
    const { tabs, tabsContent, tablistAriaLabel } = this.props;
    const { selectedTab } = this.state;

    return (
      <React.Fragment>
        <Tablist
          tabs={tabs}
          selectedTab={selectedTab}
          onTabSelect={this.handleTabSelect}
          ariaLabel={tablistAriaLabel}
        />
        {tabsContent.map(tab => (
          <TabPanel tab={tab} key={tab.id} selectedTab={selectedTab} />
        ))}
      </React.Fragment>
    );
  }
}

Tabs.propTypes = {
  tabs: PropTypes.arrayOf(PropTypes.object).isRequired,
  tabsContent: PropTypes.arrayOf(PropTypes.object).isRequired,
  tablistAriaLabel: PropTypes.string
};

export default Tabs;
