import React, { Component } from "react";
import { alphabetiseNames } from "../utils/alphabetiseNames";
import Tabs from "./common/tabs";
import Teams from "./teams";
import Loading from "./loading";

class ConferenceSelect extends Component {
  state = { selectedConference: "Eastern", isLoaded: true };

  // Conference comes back from the API as 'Eastern' rather than 'East'
  conferences = [
    { id: "Eastern", tabText: "East" },
    { id: "Western", tabText: "West" }
  ];

  getTabPanelsContent = () => {
    let tabPanels = [];

    for (let conf of this.conferences) {
      let conferenceTeams = this.props.teams.filter(
        team => team.conference === conf.id
      );

      conferenceTeams.sort((a, b) => alphabetiseNames(a, b));

      let panelObj = {
        id: conf.id,
        content: <Teams teams={conferenceTeams} />
      };
      tabPanels.push(panelObj);
    }

    return tabPanels;
  };

  handleConferenceSelect = selectedConference => {
    this.setState({ selectedConference });
  };

  render() {
    const tabPanels = this.getTabPanelsContent();

    if (this.state.isLoaded) {
      return (
        <article className="main-content">
          <div className="conferences">
            <Tabs
              tabs={this.conferences}
              tabsContent={tabPanels}
              tablistAriaLabel={"Conferences"}
            />
          </div>
        </article>
      );
    } else {
      return <Loading />;
    }
  }
}

export default ConferenceSelect;
