import React from "react";

const ConnectionError = ({ errorMessage }) => {
  return (
    <div className="main-content">
      <div className="connection-error">
        <div className="message">
          Uh oh! Looks like there was a problem connecting to the data
        </div>
        <div className="reason">Reason: {errorMessage}</div>
      </div>
    </div>
  );
};

export default ConnectionError;
