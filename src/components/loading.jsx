import React from "react";

const Loading = () => {
  return (
    <div className="loading">
      <img
        src={process.env.PUBLIC_URL + "/logos/nba-logo.svg"}
        alt=""
        className="blink"
      />
      <div className="loading-text">Loading</div>
    </div>
  );
};

export default Loading;
