import React, { Component } from "react";
import { NavLink } from "react-router-dom";

class NavBar extends Component {
  state = { menuExpanded: false };

  handleMenuToggle = () => {
    this.setState(state => ({ menuExpanded: !state.menuExpanded }));
  };

  handleLinkClick = () => {
    this.setState({ menuExpanded: false });
  };

  render() {
    const { menuExpanded } = this.state;

    return (
      <nav className="navbar">
        <div className="logo">
          <NavLink exact to="/">
            NBA App
          </NavLink>
        </div>
        <div className="hamburger">
          <button
            type="button"
            data-toggle="collapse"
            data-target="#nav-list"
            aria-controls="nav-list"
            aria-expanded={menuExpanded ? "true" : "false"}
            aria-label="Toggle navigation"
            onClick={this.handleMenuToggle}
          >
            <span />
            <span />
            <span />
          </button>
        </div>
        <div id="nav-list" className={menuExpanded ? "show" : "hide"}>
          <ul>
            <li>
              <NavLink exact to="/" onClick={this.handleLinkClick}>
                Home
              </NavLink>
            </li>
            <li>
              <NavLink exact to="/player-search" onClick={this.handleLinkClick}>
                Player Search
              </NavLink>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

export default NavBar;
