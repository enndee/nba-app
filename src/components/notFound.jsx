import React from "react";

const NotFound = () => {
  return (
    <div className="main-content">
      <div className="not-found-header">
        4<span className="screen-reader-text">0</span>
        <img src={process.env.PUBLIC_URL + "/images/basketball.svg"} alt="" />4
      </div>
      <h1>Page Not Found</h1>
    </div>
  );
};

export default NotFound;
