import React from "react";
import PlayerBio from "./playerBio";

const Player = props => {
  const { player, onPlayerSelect, selectedPlayer } = props;
  const selected = player === selectedPlayer ? true : false;

  return (
    <article className="player" onClick={() => onPlayerSelect(player)}>
      <button
        className={`player-name ${selected ? "selected" : "deselected"}`}
        aria-expanded={selected ? "true" : "false"}
        aria-controls={`player-${player.number}`}
      >
        {player.name}
      </button>
      <div
        id={`player-${player.number}`}
        className={`inside-player-bio ease ${
          player === selectedPlayer ? "show" : "hide"
        }`}
      >
        <PlayerBio key={player.id} player={player} />
      </div>
    </article>
  );
};

export default Player;
