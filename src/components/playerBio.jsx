import React from "react";

const PlayerBio = props => {
  const { player } = props;

  return (
    <div className="bio">
      <dl>
        <div className="dl-row bio-team">
          <dt>Team:</dt>
          <dd>{player.team}</dd>
        </div>
        <div className="dl-row bio-number">
          <dt>Number:</dt>
          <dd>{player.number}</dd>
        </div>
        <div className="dl-row bio-position">
          <dt>Position:</dt>
          <dd>{player.position}</dd>
        </div>
        <div className="dl-row bio-height">
          <dt>Height:</dt>
          <dd>{player.height}</dd>
        </div>
        <div className="dl-row bio-dob">
          <dt>Date of Birth:</dt>
          <dd>{player.DOB}</dd>
        </div>
      </dl>
    </div>
  );
};

export default PlayerBio;
