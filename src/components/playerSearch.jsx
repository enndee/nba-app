import React, { Component } from "react";
import { getAllPlayers } from "../services/playerService";
import { alphabetiseNames } from "../utils/alphabetiseNames";
import { paginate } from "../utils/paginate";
import SearchBox from "./common/searchBox";
import Player from "./player";
import PlayerBio from "./playerBio";
import Banner from "./banner";
import PaginationDropdown from "./common/paginationDropdown";
import ConnectionError from "./connectionError";
import Loading from "./loading";

class PlayerSearch extends Component {
  state = {
    players: [],
    searchQuery: "",
    selectedPlayer: null,
    pageSize: 10,
    currentPage: 1,
    isLoaded: false,
    isError: false,
    errorMessage: null
  };

  async componentDidMount() {
    try {
      const { data: players } = await getAllPlayers();
      this.setState({ players, isLoaded: true });
    } catch (ex) {
      this.setState({
        isLoaded: true,
        isError: true,
        errorMessage: ex.message
      });
      console.log(ex);
    }
  }

  handleSearch = searchQuery => {
    this.setState({ searchQuery, currentPage: 1, selectedPlayer: null });
  };

  getPlayerList = () => {
    const { players: allPlayers, searchQuery } = this.state;

    let filtered = allPlayers;

    if (searchQuery) {
      filtered = allPlayers.filter(player =>
        player.name.toLowerCase().includes(searchQuery.toLowerCase())
      );
      filtered.sort((a, b) => alphabetiseNames(a, b));
      return filtered;
    } else return allPlayers.sort((a, b) => alphabetiseNames(a, b));
  };

  handlePlayerSelect = selectedPlayer => {
    if (selectedPlayer === this.state.selectedPlayer) selectedPlayer = null;
    this.setState({ selectedPlayer });
  };

  handlePageChange = page => {
    this.setState({ currentPage: page });
  };

  getTeamSlug = () => {
    const { selectedPlayer } = this.state;

    const teamSlug = selectedPlayer
      ? selectedPlayer.team.replace(/\s+/g, "-").toLowerCase()
      : "nba";

    return teamSlug;
  };

  render() {
    const {
      isLoaded,
      isError,
      errorMessage,
      searchQuery,
      selectedPlayer,
      pageSize,
      currentPage
    } = this.state;

    const filtered = this.getPlayerList();
    const playerList = paginate(filtered, currentPage, pageSize);
    const teamSlug = this.getTeamSlug();

    if (isLoaded && !isError) {
      return (
        <div className={teamSlug}>
          <Banner
            logo={`${process.env.PUBLIC_URL}/logos/${teamSlug}-logo.svg`}
          />
          <article className="main-content">
            <div className="player-search-container">
              <SearchBox
                id="player-search"
                label="Enter player name"
                placeholder="Enter player name"
                value={searchQuery}
                onChange={this.handleSearch}
                size="25"
              />
            </div>
            <div className="search-results-container">
              <div className="search-results-list">
                {playerList.length > 0 ? (
                  playerList.map(player => (
                    <Player
                      key={player.id}
                      player={player}
                      selectedPlayer={selectedPlayer}
                      onPlayerSelect={this.handlePlayerSelect}
                    />
                  ))
                ) : (
                  <p>There are no players matching that name</p>
                )}
                <PaginationDropdown
                  itemsCount={filtered.length}
                  pageSize={pageSize}
                  currentPage={currentPage}
                  onPageChange={this.handlePageChange}
                />
              </div>
              {selectedPlayer && (
                <section className="outside-player-bio" aria-hidden="true">
                  <div className="player-name">{selectedPlayer.name}</div>
                  <PlayerBio player={selectedPlayer} />
                </section>
              )}
            </div>
          </article>
        </div>
      );
    } else if (isError) {
      return <ConnectionError errorMessage={errorMessage} />;
    } else return <Loading />;
  }
}

export default PlayerSearch;
