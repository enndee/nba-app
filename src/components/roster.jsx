import React, { Component } from "react";
import { alphabetiseNames } from "../utils/alphabetiseNames";
import Player from "./player";
import PlayerBio from "./playerBio";

class Roster extends Component {
  state = {
    players: [],
    selectedPlayer: null
  };

  componentDidMount() {
    const players = this.props.roster.map(player => ({
      ...player,
      show: false
    }));
    this.setState({ players });
  }

  handlePlayerSelect = selectedPlayer => {
    if (selectedPlayer === this.state.selectedPlayer) selectedPlayer = null;
    this.setState({ selectedPlayer });
  };

  render() {
    const { selectedPlayer } = this.state;

    return (
      <section className="roster">
        <h2>Roster</h2>
        <div className="roster-container">
          <div className="roster-list">
            {this.state.players
              .sort((a, b) => alphabetiseNames(a, b))
              .map(player => (
                <Player
                  key={player.id}
                  player={player}
                  selectedPlayer={selectedPlayer}
                  onPlayerSelect={this.handlePlayerSelect}
                />
              ))}
          </div>
          {selectedPlayer && (
            <section className="outside-player-bio" aria-hidden="true">
              <div className="player-name">{selectedPlayer.name}</div>
              <PlayerBio player={selectedPlayer} />
            </section>
          )}
        </div>
      </section>
    );
  }
}

export default Roster;
