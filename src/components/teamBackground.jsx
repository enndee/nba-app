import React from "react";

const TeamBackground = ({ team }) => {
  return (
    <React.Fragment>
      <section className="team-background">
        <div className="team-info">
          <h1>{team.name}</h1>
          <dl className="team-background-dl">
            <div className="dl-row">
              <dt>Conference:</dt>
              <dd>{team.conference}</dd>
            </div>
            <div className="dl-row">
              <dt>Founded:</dt>
              <dd>{team.founded}</dd>
            </div>
            <div className="dl-row">
              <dt>Arena:</dt>
              <dd>{team.arena}</dd>
            </div>
            <div className="dl-row">
              <dt>Owner:</dt>
              <dd>{team.owner}</dd>
            </div>
            <div className="dl-row">
              <dt>G League Affiliate:</dt>
              <dd>{team.g_league}</dd>
            </div>
            <div className="dl-row">
              <dt>Website:</dt>
              <dd>
                <a href={team.franchise_website}>
                  {team.franchise_website.substring(7)}
                </a>
              </dd>
            </div>
          </dl>
        </div>
      </section>
    </React.Fragment>
  );
};

export default TeamBackground;
