import React, { Component } from "react";
import { getTeam } from "../services/teamService";
import { getRoster } from "../services/playerService";
import TeamBackground from "./teamBackground";
import Roster from "./roster";
import Banner from "./banner";
import ConnectionError from "./connectionError";
import Loading from "./loading";

class TeamInfo extends Component {
  state = {
    teamBackground: {},
    roster: [],
    isLoaded: false,
    isError: false,
    errorMessage: null
  };

  async componentDidMount() {
    const { slug } = this.props.match.params;
    try {
      const { data: teamBackground } = await getTeam(slug);
      const { data: roster } = await getRoster(slug);
      this.setState({ teamBackground, roster, isLoaded: true });
    } catch (ex) {
      this.setState({
        isLoaded: true,
        isError: true,
        errorMessage: ex.message
      });
      console.log(ex);
    }
  }

  getTeamLogo = () =>
    `${process.env.PUBLIC_URL}/logos/${this.props.match.params.slug}-logo.svg`;

  render() {
    const {
      teamBackground: team,
      roster,
      isLoaded,
      isError,
      errorMessage
    } = this.state;
    const teamLogo = this.getTeamLogo();

    if (isLoaded && !isError) {
      return (
        <React.Fragment>
          <Banner logo={teamLogo} />
          <article className="main-content">
            <TeamBackground team={team} />
            <Roster roster={roster} />
          </article>
        </React.Fragment>
      );
    } else if (isError) {
      return <ConnectionError errorMessage={errorMessage} />;
    } else {
      return <Loading />;
    }
  }
}

export default TeamInfo;
