import React from "react";
import { Link } from "react-router-dom";

const Teams = ({ teams }) => {
  return (
    <section className={"teams"}>
      {teams
        .sort((a, b) => a.slug > b.slug)
        .map(team => (
          <div key={team.id} className="team">
            <Link to={`/teams/${team.slug}`}>
              <img src={team.logo} alt={team.name} />
            </Link>
          </div>
        ))}
    </section>
  );
};

export default Teams;
