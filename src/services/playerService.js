import http from "./httpService";
import { apiUrl } from "../config.json";

const route = `${apiUrl}/players`;

export function getRoster(slug) {
  const teamName = slug.split("-").join("+");
  return http.get(`${route}?team=${teamName}`);
}

export function getPlayer(id) {
  return http.get(`${route}/${id}`);
}

export function getAllPlayers() {
  return http.get(route);
}
