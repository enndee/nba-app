import http from "./httpService";
import { apiUrl } from "../config.json";

const route = `${apiUrl}/franchises`;

export function getTeams() {
  return http.get(route);
}

export function getTeam(slug) {
  const teamName = slug.split("-").join("+");
  return http.get(`${route}/${teamName}`);
}

export async function getTeamNames() {
  const { data: teams } = await getTeams();
  const teamNames = teams.map(team => team.title.rendered);
  return teamNames;
}
