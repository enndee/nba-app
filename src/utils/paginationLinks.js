import _ from "lodash";

export function paginationLinks(currentPage, pagesCount, paginationRange) {
  // All examples are based on a default of 5 page links in the pagination
  // If the number of pages is less than 5 only show links for the number of pages available
  if (pagesCount <= paginationRange) {
    // _.range(from this, up to but not including this)
    return _.range(1, pagesCount + 1);
  }
  // If the page number is less than or equal to 3, keep showing 1 thru 5 until we get to page 4 then change it to 2 thru 6
  else if (currentPage <= Math.ceil(paginationRange / 2)) {
    return _.range(1, paginationRange + 1);
  }
  // If we are getting to the end of the range don't move the range beyond the last page.
  // i.e. for 50 pages show 46 thru 50 when on page 48, 49 and 50.
  else if (currentPage > pagesCount - Math.ceil(paginationRange / 2)) {
    return _.range(pagesCount - (paginationRange - 1), pagesCount + 1);
  }
  // Otherwise show a range from 2 pages below our current page to 2 pages above it.
  // i.e. current page is 8, show 6, 7, 8, 9, 10.
  else {
    return _.range(
      currentPage - Math.floor(paginationRange / 2),
      currentPage + Math.ceil(paginationRange / 2)
    );
  }
}
